import React from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import Talent from '../screen/Talent';
import Company from '../screen/Company';
import PIC from '../screen/PIC';
import Tracker from '../screen/Tracker';

const BottomTab = () => {
  const Tab = createMaterialBottomTabNavigator();

  return (
    <Tab.Navigator
      activeColor="#fff"
      barStyle={{backgroundColor: 'pink'}}
      shifting={true}>
      <Tab.Screen
        name="Talent"
        component={Talent}
        options={{
          headerShown: false,
          tabBarColor: 'violet',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name="account-alert"
              color={color}
              size={26}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Company"
        component={Company}
        options={{
          headerShown: false,
          tabBarColor: 'red',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="bank" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="PIC"
        component={PIC}
        options={{
          headerShown: false,
          tabBarColor: 'violet',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name="account-tie"
              color={color}
              size={26}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Tracker"
        component={Tracker}
        options={{
          headerShown: false,
          tabBarColor: 'red',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name="image-search"
              color={color}
              size={26}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomTab;
