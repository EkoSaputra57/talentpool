const initialState = {
  trackerData: [],
  isLoading: false,
  modalState: false,
  modalStateEdit: false,
  trackerId: '',
  loadingMore: false,
  pageCount: 0,
};

const tracker = (state = initialState, action) => {
  switch (action.type) {
    case 'OPEN_MODAL_TRA':
      return {
        ...state,
        modalState: true,
      };
    case 'OPEN_MODAL_TRA_EDIT':
      return {
        ...state,
        modalStateEdit: true,
        trackerId: action.data.id,
      };
    case 'CLOSE_MODAL_TRA':
      return {
        ...state,
        modalState: false,
      };
    case 'CLOSE_MODAL_TRA_EDIT':
      return {
        ...state,
        modalStateEdit: false,
        trackerId: '',
      };
    case 'GET_TRACKER_DATA':
      return {
        ...state,
        isLoading: true,
      };
    case 'GET_TRACKER_DATA_SUCCESS':
      return {
        ...state,
        trackerData: action.data,
        isLoading: false,
      };
    case 'TRACKER_DATA_EDIT':
      return {
        ...state,
        isLoading: true,
      };
    case 'TRACKER_DATA_EDIT_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'TRACKER_DATA_EDIT_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'TRACKER_DATA_CREATE':
      return {
        ...state,
        isLoading: true,
      };
    case 'TRACKER_DATA_CREATE_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'TRACKER_DATA_CREATE_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'GET_MORE_TRACKER':
      return {
        ...state,
        loadingMore: true,
        pageCount: state.pageCount + 1,
      };
    case 'GET_MORE_TRACKER_SUCCESS':
      return {
        ...state,
        trackerData: [...state.trackerData, ...action.data],
        loadingMore: false,
      };
    case 'GET_MORE_TRACKER_FAILED':
      return {
        ...state,
        pageCount: state.pageCount - 1,
        loadingMore: false,
      };
    default:
      return state;
  }
};

export default tracker;
