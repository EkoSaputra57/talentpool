const initialState = {
  companyData: [],
  isLoading: false,
  modalState: false,
  modalStateEdit: false,
  companyId: '',
  loadingMore: false,
  pageCount: 0,
};

const company = (state = initialState, action) => {
  switch (action.type) {
    case 'OPEN_MODAL_COM':
      return {
        ...state,
        modalState: true,
      };
    case 'OPEN_MODAL_COM_EDIT':
      return {
        ...state,
        modalStateEdit: true,
        companyId: action.data.id,
      };
    case 'CLOSE_MODAL_COM':
      return {
        ...state,
        modalState: false,
      };
    case 'CLOSE_MODAL_COM_EDIT':
      return {
        ...state,
        modalStateEdit: false,
        companyId: '',
      };
    case 'GET_COMPANY_DATA':
      return {
        ...state,
        isLoading: true,
      };
    case 'GET_COMPANY_DATA_SUCCESS':
      return {
        ...state,
        companyData: action.data,
        isLoading: false,
      };
    case 'COMPANY_DATA_EDIT':
      return {
        ...state,
        isLoading: true,
      };
    case 'COMPANY_DATA_EDIT_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'COMPANY_DATA_EDIT_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'COMPANY_DATA_DELETE':
      return {
        ...state,
        isLoading: true,
      };
    case 'COMPANY_DATA_DELETE_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'COMPANY_DATA_DELETE_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'COMPANY_DATA_CREATE':
      return {
        ...state,
        isLoading: true,
      };
    case 'COMPANY_DATA_CREATE_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'COMPANY_DATA_CREATE_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'GET_MORE_COMPANY':
      return {
        ...state,
        loadingMore: true,
        pageCount: state.pageCount + 1,
      };
    case 'GET_MORE_COMPANY_SUCCESS':
      return {
        ...state,
        companyData: [...state.companyData, ...action.data],
        loadingMore: false,
      };
    case 'GET_MORE_COMPANY_FAILED':
      return {
        ...state,
        pageCount: state.pageCount - 1,
        loadingMore: false,
      };
    default:
      return state;
  }
};

export default company;
