const initialState = {
  talentData: [],
  isLoading: false,
  modalState: false,
  modalStateEdit: false,
  talentId: '',
  loadingMore: false,
  pageCount: 0,
};

const talent = (state = initialState, action) => {
  switch (action.type) {
    case 'OPEN_MODAL_TAL':
      return {
        ...state,
        modalState: true,
      };
    case 'OPEN_MODAL_TAL_EDIT':
      return {
        ...state,
        modalStateEdit: true,
        talentId: action.data.id,
      };
    case 'CLOSE_MODAL_TAL':
      return {
        ...state,
        modalState: false,
      };
    case 'CLOSE_MODAL_TAL_EDIT':
      return {
        ...state,
        modalStateEdit: false,
        talentId: '',
      };
    case 'GET_TALENT_DATA':
      return {
        ...state,
        isLoading: true,
      };
    case 'GET_TALENT_DATA_SUCCESS':
      return {
        ...state,
        talentData: action.data,
        isLoading: false,
      };
    case 'TALENT_DATA_EDIT':
      return {
        ...state,
        isLoading: true,
      };
    case 'TALENT_DATA_EDIT_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'TALENT_DATA_EDIT_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'TALENT_DATA_DELETE':
      return {
        ...state,
        isLoading: true,
      };
    case 'TALENT_DATA_DELETE_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'TALENT_DATA_DELETE_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'TALENT_DATA_CREATE':
      return {
        ...state,
        isLoading: true,
      };
    case 'TALENT_DATA_CREATE_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'TALENT_DATA_CREATE_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'GET_MORE_TALENT':
      return {
        ...state,
        loadingMore: true,
        pageCount: state.pageCount + 1,
      };
    case 'GET_MORE_TALENT_SUCCESS':
      return {
        ...state,
        talentData: [...state.talentData, ...action.data],
        loadingMore: false,
      };
    case 'GET_MORE_TALENT_FAILED':
      return {
        ...state,
        pageCount: state.pageCount - 1,
        loadingMore: false,
      };

    default:
      return state;
  }
};

export default talent;
