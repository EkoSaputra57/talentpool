const initialState = {
  picData: [],
  isLoading: false,
  modalState: false,
  modalStateEdit: false,
  picId: '',
  loadingMore: false,
  pageCount: 0,
};

const pic = (state = initialState, action) => {
  switch (action.type) {
    case 'OPEN_MODAL_PIC':
      return {
        ...state,
        modalState: true,
      };
    case 'OPEN_MODAL_PIC_EDIT':
      return {
        ...state,
        modalStateEdit: true,
        picId: action.data.id,
      };
    case 'CLOSE_MODAL_PIC':
      return {
        ...state,
        modalState: false,
      };
    case 'CLOSE_MODAL_PIC_EDIT':
      return {
        ...state,
        modalStateEdit: false,
        picId: '',
      };
    case 'GET_PIC_DATA':
      return {
        ...state,
        isLoading: true,
      };
    case 'GET_PIC_DATA_SUCCESS':
      return {
        ...state,
        picData: action.data,
        isLoading: false,
      };
    case 'PIC_DATA_EDIT':
      return {
        ...state,
        isLoading: true,
      };
    case 'PIC_DATA_EDIT_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'PIC_DATA_EDIT_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'PIC_DATA_DELETE':
      return {
        ...state,
        isLoading: true,
      };
    case 'PIC_DATA_DELETE_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'PIC_DATA_DELETE_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'PIC_DATA_CREATE':
      return {
        ...state,
        isLoading: true,
      };
    case 'PIC_DATA_CREATE_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'PIC_DATA_CREATE_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'GET_MORE_PIC':
      return {
        ...state,
        loadingMore: true,
        pageCount: state.pageCount + 1,
      };
    case 'GET_MORE_PIC_SUCCESS':
      return {
        ...state,
        picData: [...state.picData, ...action.data],
        loadingMore: false,
      };
    case 'GET_MORE_PIC_FAILED':
      return {
        ...state,
        pageCount: state.pageCount - 1,
        loadingMore: false,
      };
    default:
      return state;
  }
};

export default pic;
