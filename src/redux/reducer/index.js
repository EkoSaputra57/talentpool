import {combineReducers} from 'redux';
import company from './company';
import pic from './pic';
import talent from './talent';
import tracker from './tracker';

export default combineReducers({
  company,
  pic,
  talent,
  tracker,
});
