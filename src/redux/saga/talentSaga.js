import axios from 'axios';
import {takeLatest, put, select} from '@redux-saga/core/effects';
import {Alert} from 'react-native';

function* getTalentData(action) {
  try {
    console.log('actionnya', action);
    const resTalentData = yield axios.get(
      `https://glints-talent-pool.herokuapp.com/talents?limit=${action.data.limit}`,
    );
    yield put({
      type: 'GET_TALENT_DATA_SUCCESS',
      data: resTalentData.data.data,
    });
  } catch (err) {
    console.log('>>>', err);
  }
}

function* createTalentData(action) {
  try {
    console.log('create Talent data', action);
    const resCreateTalentData = yield axios({
      method: 'POST',
      url: 'https://glints-talent-pool.herokuapp.com/talents',
      data: action.data,
    });
    if (resCreateTalentData && resCreateTalentData.data) {
      console.log(resCreateTalentData.data);
      yield put({type: 'TALENT_DATA_CREATE_SUCCESS'});
      yield put({type: 'GET_TALENT_DATA'});
      Alert.alert('Status:', 'Create Talent data Success', [{text: 'Okay'}]);
    }
    console.log('Create Talent Success');
  } catch (err) {
    console.log(err);
    yield put({type: 'TALENT_DATA_CREATE_FAILED'});
    Alert.alert('Status:', 'Create Talent data Failed, Please try Again', [
      {text: 'Okay'},
    ]);
  }
}

function* editTalentData(action) {
  const id = yield select(state => state.talent.talentId);
  console.log(action);
  try {
    console.log('edit Talent data start');
    const resEditTalentData = yield axios({
      method: 'PUT',
      url: `https://glints-talent-pool.herokuapp.com/talents/${id}`,
      data: action.data,
    });
    console.log('edit Talent data success');
    yield put({
      type: 'TALENT_DATA_EDIT_SUCCESS',
    });
    yield put({type: 'GET_TALENT_DATA'});
    Alert.alert('Status:', 'Edit Talent Data Success', [
      {
        text: 'okay',
      },
    ]);
  } catch (error) {
    console.log(err);
    yield put({
      type: 'TALENT_DATA_EDIT_FAILED',
    });
    Alert.alert('Status:', 'Edit Talent data Failed, Please try Again', [
      {text: 'Okay'},
    ]);
  }
}

function* deleteTalentData(action) {
  console.log('-->', action);
  try {
    console.log('memulai delete Talent data');
    const resDeleteTalentData = yield axios({
      method: 'delete',
      url: `https://glints-talent-pool.herokuapp.com/talents/${action.id}`,
    });
    console.log('delete Talent data success');
    yield put({type: 'TALENT_DATA_DELETE_SUCCESS'});
    yield put({type: 'GET_TALENT_DATA'});
    Alert.alert('Status:', 'Delete Talent Data Success', [
      {
        text: 'okay',
      },
    ]);
  } catch (err) {
    console.log(err);
    yield put({type: 'TALENT_DATA_DELETE_FAILED'});
    Alert.alert(
      'Status: Failed',
      'Talent cannot be deleted if talent is in a tracker',
      [{text: 'I understand'}],
    );
  }
}

function* getMoreTalent(action) {
  const page = yield select(state => state.talent.pageCount);
  try {
    console.log('mulai get more talent');
    const resGetMoreTalent = yield axios.get(
      `https://glints-talent-pool.herokuapp.com/talents?page=${
        1 + page
      }&limit=5`,
    );
    yield put({
      type: 'GET_MORE_TALENT_SUCCESS',
      data: resGetMoreTalent.data.data,
    });
  } catch (err) {
    console.log('gagal bro', err);
    yield put({type: 'GET_MORE_TALENT_FAILED'});
    Alert.alert('The End', 'No More Talent to load', [{text: 'I understand'}]);
  }
}

export default function* talentSaga() {
  yield takeLatest('GET_TALENT_DATA', getTalentData);
  yield takeLatest('TALENT_DATA_CREATE', createTalentData);
  yield takeLatest('TALENT_DATA_EDIT', editTalentData);
  yield takeLatest('TALENT_DATA_DELETE', deleteTalentData);
  yield takeLatest('GET_MORE_TALENT', getMoreTalent);
}
