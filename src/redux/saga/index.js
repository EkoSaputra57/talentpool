import {all} from 'redux-saga/effects';
import talentSaga from './talentSaga';
import trackerSaga from './trackerSaga';
import companySaga from './companySaga';
import picSaga from './picSaga';

export default function* rootSagas() {
  yield all([talentSaga(), trackerSaga(), companySaga(), picSaga()]);
}
