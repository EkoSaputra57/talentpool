import axios from 'axios';
import {takeLatest, put, select} from '@redux-saga/core/effects';
import {Alert} from 'react-native';

function* getPICData(action) {
  try {
    console.log('actionnya', action);
    const resPICData = yield axios.get(
      `https://glints-talent-pool.herokuapp.com/pics?limit=${action.data.limit}`,
    );
    yield put({
      type: 'GET_PIC_DATA_SUCCESS',
      data: resPICData.data.data,
    });
  } catch (err) {
    console.log('>>>', err);
  }
}

function* createPICData(action) {
  try {
    console.log('create PIC data', action);
    const resCreatePICData = yield axios({
      method: 'POST',
      url: 'https://glints-talent-pool.herokuapp.com/pics/',
      data: action.data,
    });
    if (resCreatePICData && resCreatePICData.data) {
      console.log(resCreatePICData.data);
      yield put({type: 'PIC_DATA_CREATE_SUCCESS'});
      yield put({type: 'GET_PIC_DATA'});
      Alert.alert('Status:', 'Create PIC data Success', [{text: 'Okay'}]);
    }
    console.log('Create PIC Success');
  } catch (err) {
    Alert.alert('Status:', 'Create PIC data Failed, Please try Again', [
      {text: 'Okay'},
    ]);
    console.log(err);
    yield put({type: 'PIC_DATA_CREATE_FAILED'});
  }
}

function* editPICData(action) {
  console.log(action);
  const id = yield select(state => state.pic.picId);
  try {
    console.log('edit PIC data start');
    const resEditPICData = yield axios({
      method: 'PUT',
      url: `https://glints-talent-pool.herokuapp.com/pics/${id}`,
      data: action.data,
    });
    console.log('edit PIC data success');
    yield put({
      type: 'PIC_DATA_EDIT_SUCCESS',
    });
    yield put({type: 'GET_PIC_DATA'});
    Alert.alert('Status:', 'Edit PIC Data Success', [
      {
        text: 'okay',
      },
    ]);
  } catch (err) {
    console.log(err);
    yield put({
      type: 'PIC_DATA_EDIT_FAILED',
    });
    Alert.alert('Status:', 'Edit PIC data Failed, Please try Again', [
      {text: 'Okay'},
    ]);
  }
}

function* deletePICData(action) {
  console.log('-->', action);
  try {
    console.log('memulai delete PIC data');
    const resDeletePICData = yield axios({
      method: 'delete',
      url: `https://glints-talent-pool.herokuapp.com/pics/${action.id}`,
    });
    console.log('delete PIC data success');
    yield put({type: 'PIC_DATA_DELETE_SUCCESS'});
    yield put({type: 'GET_PIC_DATA'});
    Alert.alert('Status:', 'Delete PIC Data Success', [
      {
        text: 'okay',
      },
    ]);
  } catch (err) {
    console.log(err);
    yield put({type: 'PIC_DATA_DELETE_FAILED'});
    Alert.alert(
      'Status: Failed',
      'PIC cannot be deleted if PIC is in a tracker',
      [{text: 'I understand'}],
    );
  }
}

function* getMorePic(action) {
  const page = yield select(state => state.pic.pageCount);
  try {
    console.log('mulai get more pic');
    const resGetMore = yield axios.get(
      `https://glints-talent-pool.herokuapp.com/pics?page=${1 + page}&limit=5`,
    );
    yield put({
      type: 'GET_MORE_PIC_SUCCESS',
      data: resGetMore.data.data,
    });
  } catch (err) {
    console.log('gagal bro', err);
    yield put({type: 'GET_MORE_PIC_FAILED'});
    Alert.alert('The End', 'No More PIC to load', [{text: 'I understand'}]);
  }
}

export default function* picSaga() {
  yield takeLatest('GET_PIC_DATA', getPICData);
  yield takeLatest('PIC_DATA_CREATE', createPICData);
  yield takeLatest('PIC_DATA_EDIT', editPICData);
  yield takeLatest('PIC_DATA_DELETE', deletePICData);
  yield takeLatest('GET_MORE_PIC', getMorePic);
}
