import axios from 'axios';
import {takeLatest, put, select} from '@redux-saga/core/effects';
import {Alert} from 'react-native';

function* getTrackerData(action) {
  try {
    console.log('actionnya', action);
    const resTrackerData = yield axios.get(
      `https://glints-talent-pool.herokuapp.com/trackers?limit=${action.data.limit}&status=${action.data.status}`,
    );
    yield put({
      type: 'GET_TRACKER_DATA_SUCCESS',
      data: resTrackerData.data.data,
    });
  } catch (err) {
    console.log('>>>', err);
  }
}

function* createTrackerData(action) {
  try {
    console.log('create tracker data', action);
    const resCreateTrackerData = yield axios({
      method: 'POST',
      url: 'https://glints-talent-pool.herokuapp.com/trackers',
      data: action.data,
    });
    if (resCreateTrackerData && resCreateTrackerData.data) {
      console.log(resCreateTrackerData.data);
      yield put({type: 'TRACKER_DATA_CREATE_SUCCESS'});
      yield put({type: 'GET_TRACKER_DATA'});
      Alert.alert('Create Tracker Status:', 'Create Tracker data Success', [
        {text: 'Okay'},
      ]);
    }
    console.log('Create Tracker Success');
  } catch (err) {
    console.log(err);
    yield put({type: 'TRACKER_DATA_CREATE_FAILED'});
    Alert.alert(
      'Create Tracker Status Failed',
      'Talent has applied to this company before or you did not pick the option',
      [{text: 'Okay'}],
    );
  }
}

function* editTrackerData(action) {
  console.log(action);
  const id = yield select(state => state.tracker.trackerId);
  try {
    console.log('edit Tracker data start');
    const resEditTrackerData = yield axios({
      method: 'PUT',
      url: `https://glints-talent-pool.herokuapp.com/trackers/${id}`,
      data: action.data,
    });
    console.log('edit Tracker data success');
    yield put({
      type: 'TRACKER_DATA_EDIT_SUCCESS',
    });
    yield put({type: 'GET_TRACKER_DATA'});
    Alert.alert('Status:', 'Edit Tracker Data Success', [
      {
        text: 'okay',
      },
    ]);
  } catch (error) {
    console.log(err);
    yield put({
      type: 'TRACKER_DATA_EDIT_FAILED',
    });
    Alert.alert('Status:', 'Edit Tracker Data Failed', [
      {
        text: 'okay',
      },
    ]);
  }
}

function* getMoreTracker(action) {
  const page = yield select(state => state.tracker.pageCount);
  try {
    console.log('mulai get more tracker');
    const resGetMore = yield axios.get(
      `https://glints-talent-pool.herokuapp.com/trackers?page=${
        1 + page
      }&limit=5`,
    );
    yield put({
      type: 'GET_MORE_TRACKER_SUCCESS',
      data: resGetMore.data.data,
    });
  } catch (err) {
    console.log('gagal bro', err);
    yield put({type: 'GET_MORE_TRACKER_FAILED'});
    Alert.alert('The End', 'No More Taracker to load', [
      {text: 'I understand'},
    ]);
  }
}

export default function* trackerSaga() {
  yield takeLatest('GET_TRACKER_DATA', getTrackerData);
  yield takeLatest('TRACKER_DATA_CREATE', createTrackerData);
  yield takeLatest('TRACKER_DATA_EDIT', editTrackerData);
  yield takeLatest('GET_MORE_TRACKER', getMoreTracker);
}
