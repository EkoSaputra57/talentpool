import axios from 'axios';
import {takeLatest, put, select} from '@redux-saga/core/effects';
import {Alert} from 'react-native';

function* getCompanyData(action) {
  try {
    console.log('actionnya', action);
    const resCompanyData = yield axios.get(
      `https://glints-talent-pool.herokuapp.com/companies?limit=${action.data.limit}`,
    );
    yield put({
      type: 'GET_COMPANY_DATA_SUCCESS',
      data: resCompanyData.data.data,
    });
  } catch (err) {
    console.log('>>>', err);
  }
}

function* createCompanyData(action) {
  try {
    console.log('create company data', action);
    const resCreateCompanyData = yield axios({
      method: 'POST',
      url: 'https://glints-talent-pool.herokuapp.com/companies/',
      data: action.data,
    });
    if (resCreateCompanyData && resCreateCompanyData.data) {
      console.log(resCreateCompanyData.data);
      yield put({type: 'COMPANY_DATA_CREATE_SUCCESS'});
      yield put({type: 'GET_COMPANY_DATA'});
      Alert.alert('Status:', 'Create Company data Success', [{text: 'Okay'}]);
    }
    console.log('Create Company Success');
  } catch (err) {
    console.log(err);
    yield put({type: 'COMPANY_DATA_CREATE_FAILED'});
    Alert.alert('Status:', 'Create Company data Failed, Please try Again', [
      {text: 'Okay'},
    ]);
  }
}

function* editCompanyData(action) {
  console.log(action);
  const id = yield select(state => state.company.companyId);
  try {
    console.log('edit company data start');
    const resEditCompanyData = yield axios({
      method: 'PUT',
      url: `https://glints-talent-pool.herokuapp.com/companies/${id}`,
      data: action.data,
    });
    console.log('edit Company data success');
    yield put({
      type: 'COMPANY_DATA_EDIT_SUCCESS',
    });
    yield put({type: 'GET_COMPANY_DATA'});
    Alert.alert('Status:', 'Edit Company Data Success', [
      {
        text: 'okay',
      },
    ]);
  } catch (error) {
    console.log(err);
    yield put({
      type: 'COMPANY_DATA_EDIT_FAILED',
    });
    Alert.alert('Status:', 'Edit Company data Failed, Please try Again', [
      {text: 'Okay'},
    ]);
  }
}

function* deleteCompanyData(action) {
  console.log('-->', action);
  try {
    console.log('memulai delete company data');
    const resDeleteComData = yield axios({
      method: 'delete',
      url: `https://glints-talent-pool.herokuapp.com/companies/${action.id}`,
    });
    console.log('delete company data success');
    yield put({type: 'COMPANY_DATA_DELETE_SUCCESS'});
    yield put({type: 'GET_COMPANY_DATA'});
    Alert.alert('Status:', 'Delete Company Data Success', [
      {
        text: 'okay',
      },
    ]);
  } catch (err) {
    console.log(err);
    yield put({type: 'COMPANY_DATA_DELETE_FAILED'});
    Alert.alert(
      'Status: Failed',
      'Company data cannot be deleted if company is in a tracker',
      [{text: 'I understand'}],
    );
  }
}

function* getMoreCompany(action) {
  const page = yield select(state => state.company.pageCount);
  try {
    console.log('mulai get more company');
    const resGetMore = yield axios.get(
      `https://glints-talent-pool.herokuapp.com/companies?page=${
        1 + page
      }&limit=5`,
    );
    yield put({
      type: 'GET_MORE_COMPANY_SUCCESS',
      data: resGetMore.data.data,
    });
  } catch (err) {
    console.log('gagal bro', err);
    yield put({type: 'GET_MORE_COMPANY_FAILED'});
    Alert.alert('The End', 'No More Company to load', [{text: 'I understand'}]);
  }
}

export default function* companySaga() {
  yield takeLatest('GET_COMPANY_DATA', getCompanyData);
  yield takeLatest('COMPANY_DATA_CREATE', createCompanyData);
  yield takeLatest('COMPANY_DATA_EDIT', editCompanyData);
  yield takeLatest('COMPANY_DATA_DELETE', deleteCompanyData);
  yield takeLatest('GET_MORE_COMPANY', getMoreCompany);
}
