import React, {useEffect, useState} from 'react';
import {
  View,
  ImageBackground,
  FlatList,
  Dimensions,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  StyleSheet,
} from 'react-native';
import Header from '../component/Header';
import TrackCard from '../component/TrackCard';
import Load from '../component/Load';
import {useDispatch, useSelector} from 'react-redux';
import TrackModal from '../component/TrackModal';
import EditStatus from '../component/EditStatus';
import Btn from '../component/Btn';
import TheStat from '../helper/Status.json';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SelectDropdown from 'react-native-select-dropdown';

const Tracker = () => {
  const dispatch = useDispatch();
  const isLoadingMore = useSelector(state => state.tracker.loadingMore);
  const track_redux = useSelector(state => state.tracker.trackerData);
  const [status, setStatus] = useState();

  useEffect(() => {
    dispatch({type: 'GET_TRACKER_DATA', data: {status: '', limit: 5}});
  }, []);

  const getFilter = () => {
    dispatch({type: 'GET_TRACKER_DATA', data: {status: status, limit: 50}});
  };

  const returnFilter = () => {
    dispatch({type: 'GET_TRACKER_DATA', data: {status: '', limit: 5}});
  };

  const renderTrack = ({item, index}) => {
    return (
      <TrackCard
        status={item.status}
        createdAt={item.createdAt}
        talent={item.talent.name}
        company={item.company.name}
        pic={item.pic.name}
        onPressEdit={() => openModalEdit(item)}
      />
    );
  };

  const openModalEdit = data => {
    dispatch({
      type: 'OPEN_MODAL_TRA_EDIT',
      data: {id: data.id},
    });
  };

  const getMoreTracker = () => {
    dispatch({type: 'GET_MORE_TRACKER', data: {status: '', limit: 5}});
  };

  const openModal = () => {
    dispatch({type: 'OPEN_MODAL_TRA'});
  };

  useEffect(() => {}, [track_redux]);
  return (
    <ImageBackground
      source={require('../asset/track.jpg')}
      resizeMode="cover"
      style={{flex: 1}}>
      <View style={{backgroundColor: 'rgba(52, 52, 52, 0.8)', flex: 1}}>
        <TrackModal />
        <EditStatus />
        <Header title="Tracker" onPressCreate={() => openModal()} />

        {track_redux.length == 0 ? (
          <Load />
        ) : (
          <FlatList
            ListHeaderComponent={
              <>
                <View style={{alignItems: 'center'}}>
                  <SelectDropdown
                    data={TheStat}
                    onSelect={(selectedItem, index) => {
                      setStatus(selectedItem);
                    }}
                    defaultButtonText="Filter by Status"
                    buttonStyle={{
                      borderRadius: 5,
                      backgroundColor: 'wheat',
                      width: Dimensions.get('screen').width - 100,
                      height: 40,
                      margin: 10,
                    }}
                    buttonTextStyle={{
                      fontWeight: 'bold',
                      color: 'black',
                      fontSize: 15,
                    }}
                    dropdownIconPosition="right"
                    renderDropdownIcon={() => {
                      return <AntDesign name="down" size={15} color="black" />;
                    }}
                    dropdownStyle={{
                      borderRadius: 8,
                      backgroundColor: 'wheat',
                    }}
                    rowTextStyle={{
                      color: 'black',
                      textTransform: 'capitalize',
                      fontSize: 15,
                    }}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginBottom: 10,
                    justifyContent: 'space-evenly',
                  }}>
                  <TouchableOpacity
                    style={style.fil}
                    onPress={() => getFilter()}>
                    <Text style={style.filName}>SET FILTER</Text>
                  </TouchableOpacity>
                  {status ? (
                    <TouchableOpacity
                      onPress={() => returnFilter()}
                      style={style.fil}>
                      <Text style={style.filName}>UNSET FILTER</Text>
                    </TouchableOpacity>
                  ) : (
                    <></>
                  )}
                </View>
              </>
            }
            ListFooterComponent={
              track_redux.length < 4 ? (
                <></>
              ) : isLoadingMore ? (
                <ActivityIndicator size="large" color="grey" />
              ) : (
                <Btn title="Load More" onPress={() => getMoreTracker()} />
              )
            }
            ListFooterComponentStyle={{marginBottom: 20}}
            data={track_redux}
            keyExtractor={(elem, i) => i}
            renderItem={renderTrack}
          />
        )}
      </View>
    </ImageBackground>
  );
};

export default Tracker;

const style = StyleSheet.create({
  fil: {
    borderWidth: 1,
    borderRadius: 8,
    backgroundColor: 'violet',
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 10,
    borderColor: 'white',
    paddingTop: 10,
  },
  filName: {
    color: 'white',
    fontWeight: 'bold',
  },
});
