import React, {useEffect, useState} from 'react';
import {ImageBackground, FlatList, View, ActivityIndicator} from 'react-native';
import Header from '../component/Header';
import Card from '../component/Card';
import TheModal from '../component/TheModal';
import Load from '../component/Load';
import Btn from '../component/Btn';
import {useDispatch, useSelector} from 'react-redux';

const Talent = () => {
  const dispatch = useDispatch();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const isLoadingMore = useSelector(state => state.talent.loadingMore);
  const modalEdit_redux = useSelector(state => state.talent.modalStateEdit);
  const modal_redux = useSelector(state => state.talent.modalState);
  const talent_redux = useSelector(state => state.talent.talentData);
  useEffect(() => {
    dispatch({type: 'GET_TALENT_DATA', data: {limit: '5'}});
  }, []);

  const renderTalent = ({item, index}) => {
    return (
      <Card
        name={item.name}
        email={item.email}
        createdAt={item.createdAt}
        onPressEdit={() => openModalEdit(item)}
        onPressDelete={() => handleDelete(item)}
      />
    );
  };

  const openModalEdit = data => {
    dispatch({
      type: 'OPEN_MODAL_TAL_EDIT',
      data: {id: data.id},
    });
  };

  const handleDelete = data => {
    dispatch({
      type: 'TALENT_DATA_DELETE',
      id: data.id,
    });
  };

  const getMoreTalent = () => {
    dispatch({type: 'GET_MORE_TALENT'});
  };

  const closeModalEdit = () => {
    setName('');
    setEmail('');
    dispatch({type: 'CLOSE_MODAL_TAL_EDIT'});
  };

  const HandleEdit = () => {
    let newData = {
      name,
      email,
    };
    dispatch({type: 'TALENT_DATA_EDIT', data: newData});
    closeModalEdit();
  };

  const openModal = () => {
    dispatch({type: 'OPEN_MODAL_TAL'});
  };

  const closeModal = () => {
    setName('');
    setEmail('');
    dispatch({type: 'CLOSE_MODAL_TAL'});
  };

  const handleCreate = () => {
    let newData = {
      name,
      email,
    };
    dispatch({type: 'TALENT_DATA_CREATE', data: newData});
    closeModal();
  };

  useEffect(() => {}, [talent_redux]);

  return (
    <ImageBackground
      source={require('../asset/talent.jpg')}
      resizeMode="cover"
      style={{flex: 1}}>
      <View style={{backgroundColor: 'rgba(52, 52, 52, 0.8)', flex: 1}}>
        <TheModal
          modalState={modal_redux}
          onPressCreate={() => handleCreate()}
          ComInput={text => setName(text)}
          valueCom={name}
          EmailInput={text => setEmail(text)}
          valueEmail={email}
          onPressTrash={() => closeModal()}
          nama="Talent"
          judul="Create Talent"
        />
        <TheModal
          modalState={modalEdit_redux}
          onPressCreate={() => HandleEdit()}
          ComInput={text => setName(text)}
          valueCom={name}
          EmailInput={text => setEmail(text)}
          valueEmail={email}
          onPressTrash={() => closeModalEdit()}
          nama="Update Talent"
          judul="Edit Talent"
        />
        <Header title="Talent" onPressCreate={() => openModal()} />
        {talent_redux.length == 0 ? (
          <Load />
        ) : (
          <FlatList
            ListFooterComponent={
              talent_redux.length < 4 ? (
                <></>
              ) : isLoadingMore ? (
                <ActivityIndicator size="large" color="grey" />
              ) : (
                <Btn title="Load More" onPress={() => getMoreTalent()} />
              )
            }
            ListFooterComponentStyle={{marginBottom: 20}}
            data={talent_redux}
            keyExtractor={(elem, i) => i}
            renderItem={renderTalent}
          />
        )}
      </View>
    </ImageBackground>
  );
};

export default Talent;
