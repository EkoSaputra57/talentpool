import React, {useEffect, useState} from 'react';
import {View, ImageBackground, FlatList, ActivityIndicator} from 'react-native';
import Header from '../component/Header';
import Card from '../component/Card';
import TheModal from '../component/TheModal';
import Load from '../component/Load';
import Btn from '../component/Btn';
import {useDispatch, useSelector} from 'react-redux';

const PIC = () => {
  const dispatch = useDispatch();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const isLoadingMore = useSelector(state => state.pic.loadingMore);
  const modalEdit_redux = useSelector(state => state.pic.modalStateEdit);
  const modal_redux = useSelector(state => state.pic.modalState);
  const pic_redux = useSelector(state => state.pic.picData);
  useEffect(() => {
    dispatch({type: 'GET_PIC_DATA', data: {limit: '5'}});
  }, []);

  const renderPIC = ({item, index}) => {
    return (
      <Card
        name={item.name}
        email={item.email}
        createdAt={item.createdAt}
        onPressEdit={() => openModalEdit(item)}
        onPressDelete={() => handleDelete(item)}
      />
    );
  };

  const openModalEdit = data => {
    dispatch({
      type: 'OPEN_MODAL_PIC_EDIT',
      data: {id: data.id},
    });
  };

  const closeModalEdit = () => {
    setName('');
    setEmail('');
    dispatch({type: 'CLOSE_MODAL_PIC_EDIT'});
  };

  const handleDelete = data => {
    dispatch({
      type: 'PIC_DATA_DELETE',
      id: data.id,
    });
  };

  const getMorePic = () => {
    dispatch({type: 'GET_MORE_PIC'});
  };

  const HandleEdit = () => {
    let newData = {
      name,
      email,
    };
    dispatch({type: 'PIC_DATA_EDIT', data: newData});
    closeModalEdit();
  };

  const openModal = () => {
    dispatch({type: 'OPEN_MODAL_PIC'});
  };

  const closeModal = () => {
    setName('');
    setEmail('');
    dispatch({type: 'CLOSE_MODAL_PIC'});
  };

  const handleCreate = () => {
    let newData = {
      name,
      email,
    };
    dispatch({type: 'PIC_DATA_CREATE', data: newData});
    closeModal();
  };

  useEffect(() => {}, [pic_redux]);
  return (
    <ImageBackground
      source={require('../asset/PIC.jpg')}
      resizeMode="cover"
      style={{flex: 1}}>
      <View style={{backgroundColor: 'rgba(52, 52, 52, 0.8)', flex: 1}}>
        <Header title="PIC" onPressCreate={() => openModal()} />
        <TheModal
          modalState={modal_redux}
          onPressCreate={() => handleCreate()}
          ComInput={text => setName(text)}
          valueCom={name}
          EmailInput={text => setEmail(text)}
          valueEmail={email}
          onPressTrash={() => closeModal()}
          nama="PIC"
          judul="Create PIC"
        />
        <TheModal
          modalState={modalEdit_redux}
          onPressCreate={() => HandleEdit()}
          ComInput={text => setName(text)}
          valueCom={name}
          EmailInput={text => setEmail(text)}
          valueEmail={email}
          onPressTrash={() => closeModalEdit()}
          nama="Update PIC"
          judul="Edit PIC"
        />
        {pic_redux.length == 0 ? (
          <Load />
        ) : (
          <FlatList
            ListFooterComponent={
              pic_redux.length < 4 ? (
                <></>
              ) : isLoadingMore ? (
                <ActivityIndicator size="large" color="grey" />
              ) : (
                <Btn title="Load More" onPress={() => getMorePic()} />
              )
            }
            ListFooterComponentStyle={{marginBottom: 20}}
            data={pic_redux}
            keyExtractor={(elem, i) => i}
            renderItem={renderPIC}
          />
        )}
      </View>
    </ImageBackground>
  );
};

export default PIC;
