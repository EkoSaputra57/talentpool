import React, {useEffect, useState} from 'react';
import {View, ImageBackground, FlatList, ActivityIndicator} from 'react-native';
import Header from '../component/Header';
import Card from '../component/Card';
import TheModal from '../component/TheModal';
import Load from '../component/Load';
import Btn from '../component/Btn';
import {useDispatch, useSelector} from 'react-redux';

const Company = () => {
  const dispatch = useDispatch();
  const isLoadingMore = useSelector(state => state.company.loadingMore);
  const modal_redux = useSelector(state => state.company.modalState);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const modalEdit_redux = useSelector(state => state.company.modalStateEdit);
  const com_redux = useSelector(state => state.company.companyData);
  useEffect(() => {
    dispatch({type: 'GET_COMPANY_DATA', data: {limit: '5'}});
  }, []);

  const getMoreCom = () => {
    dispatch({type: 'GET_MORE_COMPANY'});
  };

  const renderCom = ({item, index}) => {
    return (
      <Card
        name={item.name}
        email={item.email}
        createdAt={item.createdAt}
        onPressEdit={() => openModalEdit(item)}
        onPressDelete={() => handleDelete(item)}
      />
    );
  };

  const openModalEdit = data => {
    dispatch({
      type: 'OPEN_MODAL_COM_EDIT',
      data: {id: data.id},
    });
  };

  const closeModalEdit = () => {
    setName('');
    setEmail('');
    dispatch({type: 'CLOSE_MODAL_COM_EDIT'});
  };

  const handleDelete = data => {
    dispatch({
      type: 'COMPANY_DATA_DELETE',
      id: data.id,
    });
  };

  const HandleEdit = () => {
    let newData = {
      name,
      email,
    };
    dispatch({type: 'COMPANY_DATA_EDIT', data: newData});
    closeModalEdit();
  };

  const openModal = () => {
    dispatch({type: 'OPEN_MODAL_COM'});
  };

  const closeModal = () => {
    setName('');
    setEmail('');
    dispatch({type: 'CLOSE_MODAL_COM'});
  };

  const handleCreate = () => {
    let newData = {
      name,
      email,
    };
    dispatch({type: 'COMPANY_DATA_CREATE', data: newData});
    closeModal();
  };

  useEffect(() => {}, [com_redux]);
  return (
    <ImageBackground
      source={require('../asset/com.jpg')}
      resizeMode="cover"
      style={{flex: 1}}>
      <View style={{backgroundColor: 'rgba(52, 52, 52, 0.8)', flex: 1}}>
        <TheModal
          modalState={modal_redux}
          onPressCreate={() => handleCreate()}
          ComInput={text => setName(text)}
          valueCom={name}
          EmailInput={text => setEmail(text)}
          valueEmail={email}
          onPressTrash={() => closeModal()}
          nama="Company"
          judul="Create Company"
        />
        <TheModal
          modalState={modalEdit_redux}
          onPressCreate={() => HandleEdit()}
          ComInput={text => setName(text)}
          valueCom={name}
          EmailInput={text => setEmail(text)}
          valueEmail={email}
          onPressTrash={() => closeModalEdit()}
          nama="Update Company"
          judul="Edit Company"
        />
        <Header title="Company" onPressCreate={() => openModal()} />
        {com_redux.length == 0 ? (
          <Load />
        ) : (
          <FlatList
            ListFooterComponent={
              com_redux.length < 4 ? (
                <></>
              ) : isLoadingMore ? (
                <ActivityIndicator size="large" color="grey" />
              ) : (
                <Btn title="Load More" onPress={() => getMoreCom()} />
              )
            }
            ListFooterComponentStyle={{marginBottom: 20}}
            data={com_redux}
            keyExtractor={(elem, i) => i}
            renderItem={renderCom}
          />
        )}
      </View>
    </ImageBackground>
  );
};

export default Company;
