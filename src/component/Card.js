import React, {useState} from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {View, Text, Alert, TouchableOpacity, StyleSheet} from 'react-native';
import moment from 'moment';

const Card = props => {
  const [showBox, setShowBox] = useState(true);

  const showConfirmDialog = () => {
    return Alert.alert(
      'Delete Data',
      'Are you sure you want to delete this data?',
      [
        {
          text: 'Yes',
          onPress: () => {
            setShowBox(false);
            props.onPressDelete();
          },
        },

        {
          text: 'No',
        },
      ],
    );
  };

  return (
    <View style={{alignItems: 'center'}}>
      <TouchableOpacity style={style.card}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={style.title}>{props.name}</Text>
          <View>
            <TouchableOpacity>
              <MaterialCommunityIcons
                name="pencil"
                size={26}
                style={{color: 'red', marginBottom: 10, marginTop: 10}}
                onPress={props.onPressEdit}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <MaterialCommunityIcons
                name="delete"
                size={26}
                style={{color: 'red'}}
                onPress={() => showConfirmDialog()}
              />
            </TouchableOpacity>
          </View>
        </View>
        <Text style={style.com}>Email:{props.email}</Text>
        <Text style={{color: 'grey'}}>
          {moment(props.createdAt).format('MMM Do YYYY')}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Card;

const style = StyleSheet.create({
  card: {
    paddingBottom: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    width: 360,
    borderRadius: 8,
    paddingLeft: 20,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    marginTop: 5,
    marginBottom: 10,
  },
  img: {
    width: 50,
    height: 50,
    marginBottom: 10,
    marginTop: 10,
  },
  title: {
    color: 'rgb(31,169,239)',
    fontSize: 20,
    paddingLeft: 10,
    width: 300,
    justifyContent: 'center',
    fontWeight: 'bold',
  },
  com: {
    fontSize: 16,
    width: 300,
    fontWeight: 'bold',
    color: 'grey',
  },
});
