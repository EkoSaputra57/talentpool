import React from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Modal,
  StyleSheet,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

const TheModal = props => {
  return (
    <Modal visible={props.modalState} transparent={true} animationType="slide">
      <View style={styles.modalBackground}>
        <View style={styles.containerModal}>
          <Text style={styles.headlineText}>{props.judul}</Text>
          <View>
            <Text style={styles.title}>{props.nama} Name</Text>
            <TextInput
              style={styles.inputBox}
              placeholderTextColor="grey"
              placeholder="Input name here"
              onChangeText={props.ComInput}
              value={props.valueCom}
            />
            <Text style={styles.title}>{props.nama} Email</Text>
            <TextInput
              style={styles.inputBox}
              placeholderTextColor="grey"
              placeholder="Input email here"
              onChangeText={props.EmailInput}
              value={props.valueEmail}
            />
          </View>
          <View style={{margin: 20, width: 150}}>
            <TouchableOpacity
              onPress={() => props.onPressCreate()}
              style={styles.modalButton}>
              <Text style={{color: 'white', fontWeight: 'bold'}}>Submit</Text>
            </TouchableOpacity>

            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 5,
                marginVertical: 10,
              }}>
              <AntDesign
                name="delete"
                size={23}
                color="black"
                onPress={() => props.onPressTrash()}
              />
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default TheModal;

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)',
  },
  containerModal: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F4EEE8',
    borderWidth: 4,
    borderRadius: 10,
    width: '85%',
  },
  headlineText: {
    margin: 10,
    fontWeight: 'bold',
    fontSize: 20,
    color: 'grey',
  },
  inputBox: {
    borderWidth: 1,
    borderRadius: 10,
    width: Dimensions.get('screen').width - 100,
    margin: 10,
    paddingLeft: 10,
  },
  modalButton: {
    borderWidth: 1,
    borderRadius: 10,
    width: 150,
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    backgroundColor: 'black',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    marginLeft: 20,
    color: 'grey',
  },
});
