import React from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import moment from 'moment';

const TrackCard = props => {
  return (
    <View style={{alignItems: 'center'}}>
      <TouchableOpacity style={style.card}>
        <View style={style.con}>
          <View style={{width: 300}}>
            <View style={style.con}>
              <Text style={style.title}>Status: </Text>
              {props.status == 'review' ? (
                <Text
                  style={{
                    ...style.stat,
                    color: 'orange',
                  }}>
                  {props.status}
                </Text>
              ) : props.status == 'user interview' ? (
                <Text style={{color: 'magenta', ...style.stat}}>
                  {props.status}
                </Text>
              ) : props.status == 'hr interview' ? (
                <Text style={{color: 'violet', ...style.stat}}>
                  {props.status}
                </Text>
              ) : props.status == 'offer' ? (
                <Text style={{color: 'green', ...style.stat}}>
                  {props.status}
                </Text>
              ) : props.status == 'accepted' ? (
                <Text style={{color: 'blue', ...style.stat}}>
                  {props.status}
                </Text>
              ) : props.status == 'rejected' ? (
                <Text style={{color: 'red', ...style.stat}}>
                  {props.status}
                </Text>
              ) : (
                <Text>{props.status}</Text>
              )}
            </View>
            <View style={style.con}>
              <Text style={style.title}>Talent Name: </Text>
              <Text style={style.output}>{props.talent}</Text>
            </View>
            <View style={style.con}>
              <Text style={style.title}>Company: </Text>
              <Text style={style.output}>{props.company}</Text>
            </View>
            <View style={style.con}>
              <Text style={style.title}>PIC: </Text>
              <Text style={style.output}>{props.pic}</Text>
            </View>
          </View>
          <TouchableOpacity>
            <MaterialCommunityIcons
              name="pencil"
              size={26}
              style={{color: 'red', marginBottom: 10, marginTop: 10}}
              onPress={props.onPressEdit}
            />
          </TouchableOpacity>
        </View>
        <Text style={{marginTop: 10, color: 'black'}}>
          {moment(props.createdAt).format('MMM Do YYYY')}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default TrackCard;

const style = StyleSheet.create({
  card: {
    paddingBottom: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    width: 360,
    borderRadius: 8,
    paddingLeft: 20,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    marginTop: 5,
    marginBottom: 10,
    paddingTop: 10,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    color: 'grey',
  },
  con: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  stat: {
    fontWeight: 'bold',
    fontSize: 20,
    textTransform: 'capitalize',
  },
  output: {
    color: 'black',
    fontWeight: 'bold',
  },
});
