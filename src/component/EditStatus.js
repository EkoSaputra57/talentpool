import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Modal,
  StyleSheet,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SelectDropdown from 'react-native-select-dropdown';
import {useDispatch, useSelector} from 'react-redux';
import TheStat from '../helper/Status.json';

const EditStatus = () => {
  const dispatch = useDispatch();
  const modal_redux = useSelector(state => state.tracker.modalStateEdit);
  const [status, setStatus] = useState();

  const closeModal = () => {
    setStatus('');
    dispatch({type: 'CLOSE_MODAL_TRA_EDIT'});
  };

  const handleEdit = () => {
    let newStatus = {
      status,
    };
    dispatch({type: 'TRACKER_DATA_EDIT', data: newStatus});
    closeModal();
  };

  return (
    <Modal visible={modal_redux} transparent={true} animationType="slide">
      <View style={styles.modalBackground}>
        <View style={styles.containerModal}>
          <Text style={styles.headlineText}>Update Status</Text>
          <View>
            <Text style={styles.title}>Choose Status</Text>
            <SelectDropdown
              data={TheStat}
              onSelect={(selectedItem, index) => {
                setStatus(selectedItem);
              }}
              defaultButtonText="Pick Status"
              buttonStyle={{
                borderRadius: 5,
                backgroundColor: 'wheat',
                width: Dimensions.get('screen').width - 100,
                height: 40,
                margin: 10,
              }}
              buttonTextStyle={{
                fontWeight: 'bold',
                color: 'black',
                fontSize: 15,
              }}
              dropdownIconPosition="right"
              renderDropdownIcon={() => {
                return <AntDesign name="down" size={15} color="black" />;
              }}
              dropdownStyle={{
                borderRadius: 8,
                backgroundColor: 'wheat',
              }}
              rowTextStyle={{
                color: 'black',
                textTransform: 'capitalize',
                fontSize: 15,
              }}
            />
          </View>
          <View style={{margin: 20, width: 150}}>
            <TouchableOpacity
              onPress={() => handleEdit()}
              style={styles.modalButton}>
              <Text style={{color: 'white', fontWeight: 'bold'}}>Submit</Text>
            </TouchableOpacity>

            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 5,
                marginVertical: 10,
              }}>
              <AntDesign
                name="delete"
                size={23}
                color="black"
                onPress={() => closeModal()}
              />
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default EditStatus;

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)',
  },
  containerModal: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F4EEE8',
    borderWidth: 4,
    borderRadius: 10,
    width: '85%',
  },
  headlineText: {
    margin: 10,
    fontWeight: 'bold',
    fontSize: 20,
    color: 'grey',
  },
  inputBox: {
    borderWidth: 1,
    borderRadius: 10,
    width: Dimensions.get('screen').width - 100,
    margin: 10,
    paddingLeft: 10,
  },
  modalButton: {
    borderWidth: 1,
    borderRadius: 10,
    width: 150,
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    backgroundColor: 'black',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    marginLeft: 20,
    color: 'grey',
  },
});
