import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Modal,
  StyleSheet,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SelectDropdown from 'react-native-select-dropdown';

const TrackModal = props => {
  const dispatch = useDispatch();
  const modal_redux = useSelector(state => state.tracker.modalState);
  const tal_redux = useSelector(state => state.talent.talentData);
  const com_redux = useSelector(state => state.company.companyData);
  const pic_redux = useSelector(state => state.pic.picData);
  const [talent, setTalent] = useState();
  const [pic, setPic] = useState();
  const [company, setCompany] = useState();

  function getName(item) {
    return [item.name, ' - ', item.id];
  }

  const closeModal = () => {
    setTalent('');
    setPic('');
    setCompany('');
    dispatch({type: 'CLOSE_MODAL_TRA'});
  };

  const handleCreate = () => {
    let newData = {
      talent,
      pic,
      company,
    };
    dispatch({type: 'TRACKER_DATA_CREATE', data: newData});
    closeModal();
  };

  useEffect(() => {
    dispatch({type: 'GET_TALENT_DATA', data: {limit: '500'}});
    dispatch({type: 'GET_PIC_DATA', data: {limit: '500'}});
    dispatch({type: 'GET_COMPANY_DATA', data: {limit: '500'}});
  }, []);

  useEffect(() => {}, [tal_redux]);
  useEffect(() => {}, [com_redux]);
  useEffect(() => {}, [pic_redux]);

  return (
    <Modal visible={modal_redux} transparent={true} animationType="slide">
      <View style={styles.modalBackground}>
        <View style={styles.containerModal}>
          <Text style={styles.headlineText}>Create Tracker</Text>
          <View>
            <Text style={styles.title}>Talent Name</Text>
            <SelectDropdown
              data={tal_redux.map(getName)}
              onSelect={(selectedItem, index) => {
                setTalent(selectedItem[2]);
              }}
              defaultButtonText="Pick Talent"
              buttonStyle={{
                borderRadius: 5,
                backgroundColor: 'wheat',
                width: Dimensions.get('screen').width - 100,
                height: 40,
                margin: 10,
              }}
              buttonTextStyle={{
                fontWeight: 'bold',
                color: 'black',
                fontSize: 15,
              }}
              dropdownIconPosition="right"
              renderDropdownIcon={() => {
                return <AntDesign name="down" size={15} color="black" />;
              }}
              dropdownStyle={{
                borderRadius: 8,
                backgroundColor: 'wheat',
              }}
              rowTextStyle={{
                color: 'black',
                textTransform: 'capitalize',
                fontSize: 15,
              }}
            />

            <Text style={styles.title}>Company Name</Text>
            <SelectDropdown
              data={com_redux.map(getName)}
              onSelect={(selectedItem, index) => {
                setCompany(selectedItem[2]);
              }}
              defaultButtonText="Pick Company"
              buttonStyle={{
                borderRadius: 5,
                backgroundColor: 'wheat',
                width: Dimensions.get('screen').width - 100,
                height: 40,
                margin: 10,
              }}
              buttonTextStyle={{
                fontWeight: 'bold',
                color: 'black',
                fontSize: 15,
              }}
              dropdownIconPosition="right"
              renderDropdownIcon={() => {
                return <AntDesign name="down" size={15} color="black" />;
              }}
              dropdownStyle={{
                borderRadius: 8,
                backgroundColor: 'wheat',
              }}
              rowTextStyle={{
                color: 'black',
                textTransform: 'capitalize',
                fontSize: 15,
              }}
            />
            <Text style={styles.title}>PIC name</Text>
            <SelectDropdown
              data={pic_redux.map(getName)}
              onSelect={(selectedItem, index) => {
                setPic(selectedItem[2]);
              }}
              defaultButtonText="Pick PIC"
              buttonStyle={{
                borderRadius: 5,
                backgroundColor: 'wheat',
                width: Dimensions.get('screen').width - 100,
                height: 40,
                margin: 10,
              }}
              buttonTextStyle={{
                fontWeight: 'bold',
                color: 'black',
                fontSize: 15,
              }}
              dropdownIconPosition="right"
              renderDropdownIcon={() => {
                return <AntDesign name="down" size={15} color="black" />;
              }}
              dropdownStyle={{
                borderRadius: 8,
                backgroundColor: 'wheat',
              }}
              rowTextStyle={{
                color: 'black',
                textTransform: 'capitalize',
                fontSize: 15,
              }}
            />
          </View>
          <View style={{margin: 20, width: 150}}>
            <TouchableOpacity
              onPress={() => handleCreate()}
              style={styles.modalButton}>
              <Text style={{color: 'white', fontWeight: 'bold'}}>Submit</Text>
            </TouchableOpacity>

            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 5,
                marginVertical: 10,
              }}>
              <AntDesign
                name="delete"
                size={23}
                color="black"
                onPress={() => closeModal()}
              />
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default TrackModal;

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)',
  },
  containerModal: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F4EEE8',
    borderWidth: 4,
    borderRadius: 10,
    width: '85%',
  },
  headlineText: {
    margin: 10,
    fontWeight: 'bold',
    fontSize: 20,
    color: 'grey',
  },
  inputBox: {
    borderWidth: 1,
    borderRadius: 10,
    width: Dimensions.get('screen').width - 100,
    margin: 10,
    paddingLeft: 10,
  },
  modalButton: {
    borderWidth: 1,
    borderRadius: 10,
    width: 150,
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    backgroundColor: 'black',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    marginLeft: 20,
    color: 'grey',
  },
});
