import React from 'react';
import {View, Text, ActivityIndicator} from 'react-native';

const Load = () => {
  return (
    <View
      style={{
        justifyContent: 'space-evenly',
        alignItems: 'center',
        height: '100%',
        margin: 20,
      }}>
      <ActivityIndicator size={150} color="grey" />
      <Text style={{fontSize: 20, color: 'white'}}>Please Wait</Text>
    </View>
  );
};

export default Load;
