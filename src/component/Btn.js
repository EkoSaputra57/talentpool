import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

const Btn = props => {
  return (
    <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <TouchableOpacity
        style={{
          borderRadius: 5,
          width: 200,
          height: 40,
          paddingTop: 5,
          paddingBottom: 5,
          marginTop: 10,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'violet',
        }}
        onPress={() => props.onPress()}>
        <Text style={{color: 'white', fontSize: 16, fontWeight: 'bold'}}>
          {props.title}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Btn;
